 /*
 ==============================================================================
 *   Object: Validate
 *   --------------------------------------------------------------------------
 *   1) Validates the input fields
 ==============================================================================
 */

 var Validate = {
   settings: {
     validatebutton: $('a.validate')
   },
   init: function() {
     this.validateFields();
   },
   validateFields: function($step) {
    //  console.log($step);
     if ( $step == 2 ) {
       $fieldsets = $('section.focus fieldset.radiobuttons');

       $.each($fieldsets, function() {
         var flag = false;
         $radioFields = $(this).find('input:radio');

         $.each($radioFields, function(item) {
           if ( $(this).is(':checked') ) {
             flag = true;
           }

           $(this).on('change', function(){
             $(this).closest('fieldset').find('.error-visible').removeClass('error-visible');
           });

         }); // end each

         if ( flag == false) {
           $(this).find('span.error').addClass('error-visible');
         }
         else {
           $(this).find('.error-visible').removeClass('error-visible');
         }

       }); // end each

       if ( $('section.focus input[name="alter"]').val() == '' ) {
         $('section.focus input[name="alter"]').closest('fieldset').find('.alter-error').addClass('error-visible');
       }
       $('section.focus input[name="alter"]').on('focus', function() {
         $('section.focus input[name="alter"]').closest('fieldset').find('.alter-error').removeClass('error-visible');
       });

       if ( $('section.focus input[name="gewicht"]').val() == '' ) {
         $('section.focus input[name="gewicht"]').closest('fieldset').find('.gewicht-error').addClass('error-visible');
       }
       $('section.focus input[name="gewicht"]').on('focus', function() {
         $('section.focus input[name="gewicht"]').closest('fieldset').find('.gewicht-error').removeClass('error-visible');
       });

     }

     // validate step 3
     if ($step == 3 ) {
       if($('section.focus input[name="symptome"]:checked').length <= 0) {
         $('section.focus input[name="symptome"]').parent().find('span.error').addClass('error-visible');
       } else {
         $('section.focus input[name="symptome"]').parent().find('.error-visible').removeClass('error-visible');
       }
       $('section.focus input[name="symptome"]').on('change', function(){
         $('section.focus input[name="symptome"]').parent().find('.error-visible').removeClass('error-visible');
       });
     }

     // validate symptome
     if ($step == 'symptome') {

       if ( $('section.focus input:checkbox:checked').length > 0 ) {
         $('section.focus input:checkbox').parent().parent().parent().parent().find('span.error').removeClass('error-visible');
       } else {
         $('section.focus input:checkbox').parent().parent().parent().parent().find('span.error').addClass('error-visible');
       }
       $('section.focus input:checkbox').on('change', function(){
         $('section.focus input:checkbox').parent().parent().parent().parent().parent().find('span.error').removeClass('error-visible');
       });
     }

     $error = $('.error-visible');

     // check if errors occured
     if($error.length){
       $formStatus = false;
     } else {
       $formStatus = true;
     }

     //console.log($formStatus);

     return $formStatus;

   }
 }
