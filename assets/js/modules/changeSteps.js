/*
==============================================================================
*   Object: ChangeSteps
*   --------------------------------------------------------------------------
*   1) Change of Sections
==============================================================================
*/

var ChangeSteps = {
  settings: {
    losbtn: $('a.los'),
    nextbtn: $('a.next-step'),
    nextsymptome: $('a.next-symptome')
  },

  init: function() {
    this.bindUIActions();
    $(":checkbox, :radio").labelauty({label: false});
  },

  bindUIActions: function() {

    this.settings.losbtn.on('click', function() {
      $('.logo').css('left', '10%');
      var curSection = $(this).parent().parent();
      var tarSection = $(curSection).next('section');
      ChangeSteps.animateSection(curSection, tarSection);
    });

    this.settings.nextbtn.on('click', function() {
      var curSection = $(this).parent().parent();
      var tarSection = $(curSection).next('section');
      var curStep = curSection.attr('data-step');
      var nextStep = tarSection.attr('data-step');

      var valResult = Validate.validateFields(2);
      // console.log( 'ValResult = ' + valResult );

      if ( curStep == 2) {
        if ( valResult == true ) {
          PushToDB.sendData();
          ChangeSteps.animateSection(curSection, tarSection);
        }
      }
      else {
        ChangeSteps.animateSection(curSection, tarSection);
      }

      if ( curStep == 3 ) {
        if ( valResult == true ) {
          ChangeSteps.animateSection(curSection, tarSection);
        }
      }

    });

    this.settings.nextsymptome.on('click', function() {
      var curSection = $(this).parent().parent();
      var symptome = $('input[name=symptome]:checked').val();
      var tarSection = $('section[data-step="' + symptome + '"]');

      $('section[data-step="5"]').addClass(symptome);

      var valResult = Validate.validateFields(3);
      if ( valResult == true ) {
        ChangeSteps.animateSection(curSection, tarSection);
      }
    });

  },

  animateSection: function(curSection, tarSection) {
    var animationIn = 'animated fadeIn';
    var animationOut = 'animated fadeOut';
    var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend'

    $('.overlay-img.active').fadeIn(1000).next('.overlay-img').fadeIn(1000, function() {
      $(this).addClass('active')
    });

    $(curSection).removeClass('focus');
    $(tarSection).addClass('focus');

    $(curSection).addClass(animationOut).one(animationEnd,
      function () {
        $(this).removeClass('focus show-part ' + animationOut).addClass('hide-part');
        $(tarSection).removeClass('hide-part').addClass('focus show-part ' + animationIn).one(animationEnd,
          function() {
            $(this).addClass('focus show-part').removeClass(animationIn);
          }
        );
      }
    );
  },

}
