 /*
 ==============================================================================
 *   Object: PushToDB
 *   --------------------------------------------------------------------------
 *   1) Data transfer to database
 ==============================================================================
 */

var PushToDB = {

  sendData: function() {

    var animalData = {
      "animal": $('input[name="tier"]:checked').val(),
      "sex": $('input[name="geschlecht"]:checked').val(),
      "age": $('input[name="alter"]').val(),
      "weight": $('input[name="gewicht"]').val(),
      "exercise": $('input[name="bewegung"]:checked').val(),
      "food": $('input[name="trockenfutter"]:checked').val(),
    }
    $.ajax({
      type: "post",
      url: "assets/js/ajax-db.php",
      data: {"data":animalData},
      success: function (data) {
        //console.log('Erfolg!');
      },
      error: function (data) {
        //console.log('misst');
      }
    });

  }
}
