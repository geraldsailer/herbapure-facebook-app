/*
==============================================================================
*   Object: FilterProducts
*   --------------------------------------------------------------------------
*   1) Filters products for being shown in products section
==============================================================================
*/

var Filter = {
  settings: {
    filterbtn: $('a.filter-products'),
  },

  init: function() {
    this.filterProducts();
  },

  filterProducts: function() {
    this.settings.filterbtn.on('click', function() {
      var curSection = $(this).parent().parent();
      var tarSection = $('section[data-step="5"]');
      var productQuery = [];

      $('.produkt').addClass('hidden');

      // search for checked terms and push to array
      $('section.focus input').each(function() {
        if ( $(this).is(':checked')) {
          productQuery.push ( $(this).val() );
        }
      });

      productQuery.forEach(function(item) {
        $('.produkt[data-symptoms*="' + item + '"]').removeClass('hidden');
      });

      // center elements if there are only 1 or 2 results
      var countProducts = $('.produkt').not('.hidden').length;
      var visibleProducts = $('.produkt').not('.hidden');

      if ( countProducts == 2 ) {
        $(visibleProducts).each(function() {
          $(this).addClass('col-sm-push-2 col-md-push-2');
        });
      }
      if ( countProducts == 1 ) {
        $(visibleProducts).each(function() {
          $(this).addClass('col-sm-push-4 col-md-push-4');
        });
      }
      if ( countProducts > 3 ) {
        tarSection.addClass('additional-space-top');
      }

      var valResult = Validate.validateFields('symptome');

      if ( valResult == true ) {
        ChangeSteps.animateSection(curSection, tarSection);
      }

    });
  },

}
