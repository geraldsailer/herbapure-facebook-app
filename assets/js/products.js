products = {
  calming: {
    "title": "Calming – die Ausgleichende #hp49",
    "description": "Dieses Produkt bietet Ihrem Haustier die ideale Kombination sämtlicher Naturmittel gegen Stress. Kein Wunder, dass diese Konzentration an Wirkstoffen für Gelassenheit sorgt und den Druck von Geist und Körper Ihres Lieblings nimmt.",
    "img": "calming-hp49.jpg",
    "url": "",
    "symptoms": ""
  },
  immunbooster: {
    "title": "Immun Booster – der Stärkende #hp40",
    "description": "Immunschwäche entsteht auch bei unseren Haustieren durch zu viele Stoffe, die nicht in den Körper gehören. Dieses Produkt hilft dabei diese Schadstoffe zu binden und auszuscheiden. Es wirkt sich somit positiv auf die Gesundheit Ihres Tieres aus, indem es die Abwehrkräfte stärkt.",
    "img": "immun-booster-hp40.jpg",
    "url": "",
    "symptoms": ""
  },
  weightcontrol: {
    "title": "Weight Control – der Schlankmacher #hp41",
    "description": "Ausgewogene und gesunde Ernährung ist auch bei unseren Haustieren die Voraussetzung um Übergewicht zu verhindern. Sollten es dennoch ein „paar Gramm“ zu viel sein, so hilft dieses Produkt das Körpergewicht zu regulieren, da es den Stoffwechsel und die Kalorienverbrennung aktiviert.",
    "img": "weight-control-hp41.jpg",
    "url": "",
    "symptoms": ""
  },
  magenwohl: {
    "title": "Magenwohl - der Beruhiger #hp48",
    "description": "Magenschmerzen sind auch bei unseren Haustieren ein häufig auftretendes Problem. Das damit verbundene Unwohlsein und Erbrechen kann durch diese Tropfen gemindert werden und das Abklingen von Entzündungen nach Paracelsus begünstigen.",
    "img": "magenwohl-hp48.jpg",
    "url": "",
    "symptoms": ""
  },
  seniorvital: {
    "title": "Senior Vital – der Jungbrunnen #hp43",
    "description": "Wenn Hunde und Katzen älter werden, dann haben sie, wie auch wir,	das eine oder andere „Wehwehchen“. Dieses Produkt hilft dabei die	Abwehrkräfte Ihres Lieblings zu stärken und fördert zusätzlich die Vitalität.",
    "img": "senior-vital-hp43.jpg",
    "url": "",
    "symptoms": ""
  },
  wurmex: {
    "title": "Wurmex – der Reiniger #hp45",
    "description": "Manche unserer Lieblinge haben ungebetene Gäste. Diese „Besiedelung“ kann zu gesundheitlichen Problemen führen. Die Einnahme der Tropfen	beugt einem Befall vor und wirkt außerdem präventiv und reinigend.",
    "img": "wurmex-hp45.jpg",
    "url": "",
    "symptoms": ""
  },
  painstop: {
    "title": "Pain Stop – der Schmerzlinderer #hp44",
    "description": "Früher war es normal, dass die Tiere bei Schmerzen die notwendigen Pflanzen in der Natur fanden und fraßen. Heute ist das leider nicht mehr möglich und daher muss der Mensch für diesen Ausgleich sorgen.	Die Pflanzenkombination mit ihrer beruhigenden, lindernden und schmerzstillenden Wirkung, ist das ideale Mittel dafür.",
    "img": "pain-stop-hp44.jpg",
    "url": "",
    "symptoms": "gelenkschmerzen"
  },
  gelenkvital: {
    "title": "Gelenk Vital – der Mobilisierende #hp47",
    "description": "Knochen und Knorpel werden im Laufe der Jahre auch bei unseren Haustieren schwächer. Dieses Produkt festigt die Knochen und fördert den Knorpelaufbau.",
    "img": "gelenk-vital-hp47.jpg",
    "url": "",
    "symptoms": "gelenkschmerzen"
  },
  heartpower: {
    "title": "Heart Power – der Kraftspender #hp46",
    "description": "Herz- und Kreislaufprobleme sind auch bei unseren Haustieren ein häufig auftretendes Problem. Die Kombination dieser Pflanzen stärkt das Herz und normalisiert den Kreislauf.",
    "img": "heart-power-hp46.jpg",
    "url": "",
    "symptoms": ""
  },
  darmwohl: {
    "title": "Darmwohl – der Wohltuende #hp42",
    "description": "Häufige Störungen im Darmbereich, wie Durchfall, Verstopfung oder Blähungen, werden bei unseren Lieblingen durch diese Pflanzenkombination positiv beeinflusst.	Leicht verdauliche Nahrung und ausreichend Flüssigkeit unterstützen diese Wirkung.",
    "img": "darmwohl-hp42.jpg",
    "url": "",
    "symptoms": ""
  }
}
